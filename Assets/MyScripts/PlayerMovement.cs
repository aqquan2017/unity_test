﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public float speed;
    public Camera cam;

    private Animator anim;
    private void Start()
    {
        //print("transform.forward: " + transform.forward);
        //print("transform.up: " + transform.up);
        //print("transform.right: " + transform.right);

        //print("TEST MAG " + transform.forward.magnitude);

        anim = GetComponent<Animator>();
        GameEvent.instance.Jump_BtnClick += Jump;
    }

    private void Update()
    {
        Move();
    }

    private void Move()
    {
        if (InputSystem.instance.JoystickVector.magnitude > 0)
        {

            //Move Player pos
            transform.Translate(new Vector3(InputSystem.instance.JoystickVector.x, 0, 
                InputSystem.instance.JoystickVector.y) * speed * Time.deltaTime, cam.transform);


            transform.position = new Vector3(transform.position.x, 0, transform.position.z);

            //make camera follow player
            float previousY = cam.transform.position.y;
            cam.transform.Translate(new Vector3(InputSystem.instance.JoystickVector.x, 0, 
                InputSystem.instance.JoystickVector.y) * speed * Time.deltaTime,cam.transform);
            cam.transform.position = new Vector3(cam.transform.position.x, previousY, cam.transform.position.z);


            //Rotate camera
            transform.eulerAngles = new Vector3(0, InputHandle(), 0);

            anim.SetBool("Run", true);
        }
        else
        {
            anim.SetBool("Run", false);
        }
    }

    private float InputHandle()
    {
        float x = InputSystem.instance.JoystickVector.x;
        float y = InputSystem.instance.JoystickVector.y;

        float rotateDeg = Mathf.Rad2Deg * Mathf.Atan2(x, y);
        
        return rotateDeg + cam.transform.eulerAngles.y;
    }

    private void OnDestroy()
    {
        GameEvent.instance.Jump_BtnClick -= Jump;
    }


    private void Jump()
    {
        if (!InputSystem.instance.IsJumpButtonPressed)
        {
            anim.SetTrigger("Jump");
        }
    }
}
