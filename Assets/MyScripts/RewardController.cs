﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEditor.Experimental.UIElements;
using UnityEngine;

public class RewardController : MonoBehaviour
{
    public readonly int totalReward = 3;
    public GameObject effectPrefab;

    private int curReward =0;

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Untagged") return;

        if(other.gameObject.CompareTag(other.gameObject.name))
        {
            //Debug.Log("???");
            GameObject effPre = Instantiate(effectPrefab, other.transform.position, other.transform.rotation);
            ParticleSystem parts = effPre.GetComponent<ParticleSystem>();
            float totalDuration = parts.main.duration + parts.main.startLifetime.constant;
            Destroy(effPre, totalDuration);

            other.gameObject.SetActive(false);
            curReward++;
            CheckTheGoal();
            //Destroy(effPre);
        }
    }

    private void CheckTheGoal()
    {
        if(curReward == totalReward)
        {
            GameEvent.instance.WinUI_Display();
        }
    }
}
