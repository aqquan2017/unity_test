﻿using SimpleInputNamespace;
using UnityEngine;
using UnityEngine.UI;

public class ListenBtn : MonoBehaviour
{
    private Button btn;
    private ButtonInputUI btn_input;

    void Start()
    {
        btn = GetComponent<Button>();
        btn_input = GetComponent<ButtonInputUI>();

        Btn_AddListener();
    }


    private void Btn_AddListener()
    {
        if (!btn_input.button.value)
        {
            string cmd = btn_input.button.Key;
            if (cmd == "Camera")
            {
                btn.onClick.AddListener(GameEvent.instance.Camera_Btn); 
            }
            if (cmd == "Jump")
            {
                btn.onClick.AddListener(GameEvent.instance.Jump_Btn);
            }
        }

    }
    
}
