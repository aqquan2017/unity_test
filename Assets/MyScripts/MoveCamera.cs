﻿using System.Collections;
using UnityEngine;

public class MoveCamera : MonoBehaviour
{
    private bool btn_state = true;
    public GameObject cameraA;
    public GameObject cameraB;

    private Camera depth_of_field;
    [Range(1,2)]
    public float move_duration;

    private void Start()
    {
        depth_of_field = GetComponent<Camera>();
        GameEvent.instance.Camera_BtnClick += Click_Btn;
    }

    private void OnDestroy()
    {
        GameEvent.instance.Camera_BtnClick -= Click_Btn;
    }

    private void Click_Btn()
    {
        StopAllCoroutines();
        if (btn_state)
        {
            StartCoroutine(MoveAtoB(gameObject , cameraB, move_duration));
        }
        else
        {
            StartCoroutine(MoveAtoB(gameObject, cameraA, move_duration));
        }
        btn_state = !btn_state;
    }

    //Move obj linearly (position, rotation, deepth of field)
    private IEnumerator MoveAtoB(GameObject obj, GameObject destination, float duration)
    {
        float currentTime = 0;
        float startDepth = obj.GetComponent<Camera>().fieldOfView;
        float desDepth = destination.GetComponent<Camera>().fieldOfView;

        //reinit change value to separate Gameobj
        Vector3 startPos = obj.transform.position;
        Quaternion startRot = obj.transform.rotation;
        
        while (currentTime < duration)
        {
            currentTime += Time.deltaTime;
            obj.transform.position = Vector3.Slerp(startPos, destination.transform.position, currentTime / duration);
            obj.transform.rotation = Quaternion.Slerp(startRot, destination.transform.rotation, currentTime / duration);
            depth_of_field.fieldOfView = Mathf.Lerp( startDepth , desDepth, (currentTime / duration) );
            yield return null;
        }
    }
}
