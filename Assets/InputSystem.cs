﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SimpleInputNamespace;

public class InputSystem : MonoBehaviour
{
    public static InputSystem instance;

    private void Awake()
    {
        if (instance == null) instance = this;
        else Destroy(gameObject);
    }

    private Vector2 joystickVector;
    private bool isJumpButtonPressed;
    private bool isCameraButtonPressed;

    public Vector2 JoystickVector => joystickVector;
    public bool IsJumpButtonPressed  => isJumpButtonPressed;
    public bool IsCameraButtonPressed => isCameraButtonPressed;


    private void Update()
    {
        joystickVector = new Vector2(SimpleInput.GetAxis("Horizontal"), SimpleInput.GetAxis("Vertical"));
        isJumpButtonPressed = SimpleInput.GetButtonUp("Jump");
        isCameraButtonPressed = SimpleInput.GetButtonUp("Camera");
    }
}
